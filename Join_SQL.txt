1.buat Database

 

	create Database nama_database

 
2. Mengambil data users
   Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.

	SELECT name,email FROM users;

3.Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

	SELECT * FROM items WHERE price > 1000000;


4.Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
	 
	SELECT * FROM items WHERE name LIKE 'watch%';

5.Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items. Berikut contoh tampilan data yang ingin didapatkan

	SELECT items.id, items.name ,items.description, items.price, items.stock, items.category_id, categories.id, categories.name as kategori FROM items INNER JOIN categories on items.category_id = categories.id;

6.Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 2

	UPDATE items set price=2500000 where id = 1;
	